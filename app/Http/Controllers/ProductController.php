<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderByRaw('updated_at DESC')->get();
        return view('product')->with('product', $products);
    }
    /**
     * Get User's Products
     */
    public function userProduct()
    {
        if (auth::check()) {
            $products = Product::where('userid', '=', auth()->id())->orderByRaw('updated_at DESC')->get();
            return view('userProducts')->with('products', $products);
        } else {
            return view('products');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth::check()) {
            return view('newProduct');
        } else {
            return view('product');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth::check()) {
            request()->validate([
                'img1' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
                'img2' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
                'img3' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            ]);
            $imageName1 = Str::random(50) . '.' . request()->img1->getClientOriginalExtension();
            request()->img1->move(public_path('img/upload/product'), $imageName1);
            $imageName2 = Str::random(50) . '.' . request()->img2->getClientOriginalExtension();
            request()->img2->move(public_path('img/upload/product'), $imageName2);
            $imageName3 = Str::random(50) . '.' . request()->img3->getClientOriginalExtension();
            request()->img3->move(public_path('img/upload/product'), $imageName3);
            $id = Product::insertGetId(
                [
                    'name' => request()->name,
                    'price' => request()->price,
                    'img1' => $imageName1,
                    'img2' => $imageName2,
                    'img3' => $imageName3,
                    'userid' => auth()->id(),
                    'catid' => request()->catid
                ]
            );
            return redirect()->route('product.show', $id);
        } else {
            return view('product');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->firstOrFail();
        $alsoProducts = Product::where('id', '!=', $id)->inRandomOrder()->take(10)->get();
        $blogs = Blog::inRandomOrder()->take(10)->get();
        return view('singleProduct')->with([
            'product' => $product, 'alsoProducts' => $alsoProducts, 'blogs' => $blogs
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth::check()) {
            $product = Product::where([['id', $id], ['userid', auth()->id()]])->firstOr(function () {
                return view('product');
            });
            return view('editProduct')->with('product', $product);
        } else {
            return view('product');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth::check()) {
            $prod = Product::where([['id', $id], ['userid', auth()->id()]])->firstOr(function () {
                return view('product');
            });
            $prod->name = request('name');
            $prod->price = request('price');
            $prod->catid = request('catid');
            $prod->save();
            return redirect()->route('product.show', $id);
        } else {
            return view('product');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth::check()) {
            $products = Product::where([
                ['id', $id],
                ['userid', auth()->id()]
            ])->firstOr(function () {
                return view('product');
            })->delete();
            $products = Product::where('userid', '=', auth()->id())->orderBy("updated_at")->get();
            return view('userProducts')->with('products', $products);
        } else {
            return view('product');
        }
    }
}
