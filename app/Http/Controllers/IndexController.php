<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;

class IndexController extends Controller
{
    public function index()
    {
        $products1 = Product::inRandomOrder()->orderByRaw('updated_at DESC')->take(10)->get();
        $cakes =  Product::inRandomOrder()->where('catid', '=', 1)->orderByRaw('updated_at DESC')->take(10)->get();
        $breads =  Product::inRandomOrder()->where('catid', '=', 2)->orderByRaw('updated_at DESC')->take(10)->get();
        $pies =  Product::inRandomOrder()->where('catid', '=', 3)->orderByRaw('updated_at DESC')->take(10)->get();
        $blog = Blog::inRandomOrder()->orderByRaw('updated_at DESC')->take(10)->get();
        return view('index')->with(
            [
                'products1' => $products1,
                'cakes' => $cakes,
                'breads' => $breads,
                'pies' => $pies,
                'blog' => $blog
            ]
        );
    }
}
