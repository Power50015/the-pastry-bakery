<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class blogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Blog::orderByRaw('updated_at DESC')->get();
        return view('blog')->with('posts', $posts);
    }
    /**
     * Get User's Posts
     */
    public function userBlog()
    {
        if (auth::check()) {
            $posts = Blog::where('userid', '=', auth()->id())->orderByRaw('updated_at DESC')->get();
            return view('userBlog')->with('posts', $posts);
        } else {
            return view('blog');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth::check()) {
            return view('newBlog');
        } else {
            return view('blog');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth::check()) {
            request()->validate([
                'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            ]);
            $imageName = time() . '.' . request()->img->getClientOriginalExtension();
            request()->img->move(public_path('img/upload/blog'), $imageName);
            $id = Blog::insertGetId(
                [
                    'title' => request()->title,
                    'des' => request()->description,
                    'img' => $imageName,
                    'content' => request()->content,
                    'userid' => auth()->id()
                ]
            );
            return redirect()->route('blog.show', $id);
        } else {
            return view('blog');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Blog::where('id', $id)->firstOrFail();
        $alsoBlog = Blog::where('id', '!=', $id)->inRandomOrder()->take(10)->get();
        $products = Product::inRandomOrder()->take(10)->get();
        return view('singleBlog')->with([
            'post' => $post, 'alsoBlog' => $alsoBlog, 'products' => $products
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth::check()) {
            $blog = Blog::where([['id', $id], ['userid', auth()->id()]])->firstOr(function () {
                return view('blog');
            });
            return view('editBlog')->with('blog', $blog);
        } else {
            return view('blog');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth::check()) {
            $blog = Blog::where([['id', $id], ['userid', auth()->id()]])->firstOr(function () {
                return view('blog');
            });
            $blog->title = request('title');
            $blog->des = request('description');
            $blog->content = request('content');
            $blog->save();
            return redirect()->route('blog.show', $id);
        } else {
            return view('blog');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth::check()) {
            $post = Blog::where([
                ['id', $id],
                ['userid', auth()->id()]
            ])->firstOr(function () {
                return view('blog');
            })->delete();
            $posts = Blog::where('userid', '=', auth()->id())->orderBy("updated_at")->get();
            return view('userBlog')->with('posts', $posts);
        } else {
            return view('blog');
        }
    }
}
