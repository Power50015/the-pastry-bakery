<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function sendMsg(Request $request)
    {
        Contact::insertGetId(
            [
                'name' => request('name'),
                'email' => request('email'),
                'subject' => request('subject'),
                'msg' => request('msg')
            ]
        );
        return view('msg')->with('msg',"Thank's For Sending your message <br> <br> <br>You will Be Redirect to the main after 5 Second" );
    }
}
