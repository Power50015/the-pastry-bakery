<?php

namespace App\Http\Controllers;

use App\Subscribe;
use Illuminate\Http\Request;


class SubscribeController extends Controller
{
    public function subscribe(Request $request)
    {

        if (Subscribe::where('email', request('subscribe'))->first()) {
            $msg = "you already Subscriber";

            return view('msg')->with('msg', $msg);
            
        } else {
            Subscribe::insertGetId(
                [
                    'email' => request('subscribe')
                ]
            );
            $msg = "Thank's For Subscribe Us<br> <br> <br>You will Be Redirect to the main after 5 Second";
            return view('msg')->with('msg', $msg);
        }
    }
}
