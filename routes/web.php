<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/', 'IndexController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('dashboard');
Route::resource('blog', 'BlogController');
Route::get('userBlog', 'BlogController@userBlog')->name('userBlog');
Route::get('userProduct', 'ProductController@userProduct')->name('userProduct');
Route::resource('/product', 'ProductController');
Route::get('/about', function () {
   return view('about');
});
Route::get('/contact', function () {
    return view('contact');
 });
Route::post('contact', 'ContactController@sendMsg')->name('contact');
Route::post('/subscribe', 'SubscribeController@subscribe')->name('subscribe');
Route::resource('cart', 'CartController');
Route::resource('order', 'OrderController');
