require("./bootstrap");
require("./owl.carousel");
require("./trix");
var owl = $(".owl-carousel");
owl.owlCarousel({
    
    loop: true,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    responsive:{
        0:{
            items:1
        },
        750:{
            items: 2
        },
        1200:{
            items: 4
        }
    }
});
$(function () {
    $('.tool').tooltip()
  })

