<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'ThePastryBakery') }}</title>
        <link rel="icon" href="{{ asset('/img/favicon.ico') }}" type="image/x-icon"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <header>
            @yield('layouts.tobBar', View::make('layouts.tobBar'))
            @yield('layouts.nav', View::make('layouts.nav'))
        </header>
        <main class="h-100vh">
            @yield('content')
        </main>
        <footer class="bg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="w-100">
                            <img src="{{ asset('img/logo.png') }}" alt="Logo" width="150" class="my-3">
                            <p class="color-1">Soufflé danish gummi bears tart. Pie wafer icing. Gummies jelly beans powder. Chocolate bar pudding macaroon candy canes chocolate apple pie chocolate cake. Sweet caramels sesame snaps halvah bear claw wafer. Sweet roll soufflé muffin topping muffin brownie…</p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 text-center">
                        <ul class="my-5">
                            <li><a class="nav-link color-red" href="{{url('/')}}">Home</a></li>
                            <li><a class="nav-link color-red" href="{{url('/product')}}">Products</a></li>
                            <li><a class="nav-link color-red" href="{{url('/blog')}}">Blog</a></li>
                            <li><a class="nav-link color-red" href="{{url('/about')}}">About Us</a></li>
                            <li><a class="nav-link color-red" href="{{url('/contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-4 mt-5 color-1">
                        <h5>Subscribe Email</h5>
                        <h6>Give us your email, and we shall send regular updates for new stuff and events.</h6>
                        <form action="{{route('subscribe')}}" method="post">
                            @csrf
                            <input type="email" name="subscribe" id="subscribe" class="w-100 form-control">
                            <button type="submit" class="btn bg-1 color-4 w-100 mt-3">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
        <div class="w-100"><div class="container color-1"> All rights reserved</div></div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
