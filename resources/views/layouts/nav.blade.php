<div class="w-100 bg-4">
  <div class="d-flex justify-content-center logo">
    <img src="{{ asset('img/logo.png') }}" alt="Logo" width="150" class="my-3">
  </div>
</div>
<nav class="navbar navbar-expand-lg bg-4">
  <div class="container pt-2 border-color-3 border-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <img src="{{ asset('img/down-arrow.png') }}" alt="nav-btn" width="25">
    </button>
    <div class="collapse navbar-collapse " id="navbarSupportedContent">
      <ul class="navbar-nav w-100 d-flex justify-content-center text-center">
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link" href="{{url('/')}}">Home</a>
        </li>
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link" href="{{url('/blog')}}">Blog</a>
        </li>
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link" href="{{url('/product')}}">Products</a>
        </li>
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link" href="{{url('/about')}}">About Us</a>
        </li>
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link" href="{{url('/contact')}}">Contact Us</a>
        </li>
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link color-red" href="{{url('/cart')}}">Cart :{{ Cart::count()}}</a>
        </li>
        @auth
        <li class="nav-item mx-4 my-3 my-lg-1 dropdown">
          <a class="nav-link dropdown-toggle color-red" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{Auth::user()->name}}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{route('dashboard')}}">Dashboard</a>
            <a class="dropdown-item" href="{{url('/userProduct')}}">Products</a>
            <a class="dropdown-item" href="{{route('userBlog')}}">Blogs</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item color-red" href="{{ url('/logout') }}">Logout</a>
          </div>
        </li>
        @else
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link color-red" href="{{url('/login')}}">Login</a>
        </li>
        <li class="nav-item mx-4 my-3 my-lg-1">
          <a class="nav-link color-red" href="{{url('/register')}}">Register</a>
        </li>
        @endauth
      </ul>
    </div>
  </div>
</nav>