<div class="tob-bar w-100 border-color-3 border-bottom bg-1 py-2">
    <div class="container">
        <div class="row">
            <div class="col-6 text-left color-2">
                460 West 34th Street, 15th floor, New York
            </div>
            <div class="col-6 text-right color-2">
                Hotline: 804-377-3580 - 804-399-3580
            </div>
        </div>
    </div>
</div>