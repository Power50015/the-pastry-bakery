@extends('layouts.app')

@section('content')
<div class="slide-page w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container">
        <h1 class="text-center color-1">Blog</h1>
    </div>
</div>
<div class="container blogs my-5">
    <div class="row">
        @forelse ($posts as $post)
            <div class="col-12 col-lg-4 mb-3">
                <div class="card w-100">
                    <img src="{{asset('img/upload/blog/' .  $post->img)}}" class="card-img-top w-100" alt="..." height="250">
                    <div class="card-body">
                      <h5 class="card-title">{{ $post->title}}</h5>
                      <p class="card-text">{{ $post->des}} ...</p>
                      <a href="{{route('blog.show',$post->id)}}" class="btn btn-primary w-100 bg-1 border-color-1">Read More ...</a>
                    </div>
                  </div>
            </div>
        @empty
            There is now Posts
        @endforelse
    </div>
</div>
@endsection
