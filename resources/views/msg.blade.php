@extends('layouts.app')

@section('content')
<div class="slide-page w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container">
        <h1 class="text-center color-1">Thanks a lot</h1>
    </div>
</div>
<div class="container blogs my-5">
    <div class="row">
        <h2 class="text-center w-100 mt-3">{!!$msg!!}</h2>
    </div>
</div>

<script>
//Using setTimeout to execute a function after 5 seconds.
setTimeout(function () {
   //Redirect with JavaScript
   window.location.href= '{{URL::to('/')}}';
}, 5000);
</script>
@endsection
