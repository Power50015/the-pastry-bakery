@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{route('product.create')}}" class="btn w-100 color-4 bg-1 border-color-1">Add Product</a>
                        </div>
                        <div class="col-12 mt-2">
                            <a href="{{route('blog.create')}}" class="btn w-100 color-4 bg-2 border-color-2">Add Post</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
