@extends('layouts.app')

@section('content')
<div class="slide-page w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container">
        <h1 class="text-center color-1">Products</h1>
    </div>
</div>
<div class="container blogs my-5">
    <div class="row product-list">
        @forelse ($product as $product)
            <div class="col-12 col-lg-4 mb-3">
                <div class="item position-relative" style="">
                    <img src="{{asset('img/upload/product/' .  $product->img1)}}" alt="{{$product->name}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute py-5 text-center color-1">
                        <h4 class="mt-2">{{$product->name}}</h4>
                        <h5>{{$product->price}}</h5>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('product.show',$product->id)}}" class="color-red tool fa fa-eye font-big" title="Show"></a>
                            </div>
                            <div class="col-6">
                                <form method="post" action="{{ route('cart.store')}}">
                                    {{ csrf_field() }}
                                    <input name="id" type="hidden" value="{{$product->id}}">
                                    <input name="name" type="hidden" value="{{$product->name}}">
                                    <input name="qty" value="1" type="hidden" value="{{$product->qty}}">
                                    <input name="price" type="hidden" value="{{$product->price}}">
                                    <button class="color-red tool btn fa fa-cart-plus font-big p-0" title="Add To Cart" type="submit">
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            There is now products
        @endforelse
    </div>
</div>
@endsection
