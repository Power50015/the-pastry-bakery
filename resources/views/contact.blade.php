@extends('layouts.app')

@section('content')
<div class="slide-page w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container">
        <h1 class="text-center color-1">Contact Us</h1>
    </div>
</div>
<div class="container my-5">
        <h2 class="color-1 mb-5">Social Media</h2>
        <div class="col-12">
            <div class="row text-center">
                <div class="col-2">
                    <a href="#" class="color-1 fa fa-facebook font-big tool" title="Facebook"></a>
                </div>
                <div class="col-2">
                    <a href="#" class="color-1 fa fa-twitter font-big tool" title="Twitter"></a>
                </div>
                <div class="col-2">
                    <a href="#" class="color-1 fa fa-instagram font-big tool" title="Instagram"></a>
                </div>
                <div class="col-2">
                    <a href="#" class="color-1 fa fa-snapchat-ghost font-big tool" title="Snapchat"></a>
                </div>
                <div class="col-2">
                    <a href="#" class="color-1 fa fa-whatsapp font-big tool" title="Whatsapp"></a>
                </div>
                <div class="col-2">
                    <a href="#" class="color-1 fa fa-envelope-o font-big tool" title="Email"></a>
                </div>
            </div>
        </div>
        <form action="{{route('contact')}}" method="POST" class="col-12 mt-5">
            <h2 class="color-1">Send You Message</h2>
            @csrf
            <div class="form-group">
                <label for="exampleInputName1">Your Name</label>
                <input type="text" name="name" class="form-control" id="exampleInputName1" aria-describedby="nameHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                    else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputSubject1">Subject</label>
                <input type="text" name="subject" class="form-control" id="exampleInputSubject1" aria-describedby="SubjectHelp">
            </div>
            <div class="form-group">
                <label for="exampleInputMessage1">Your Message</label>
                <textarea class="form-control" id="exampleInputMessage1" name="msg" aria-describedby="messageHelp" rows="10"></textarea>
            </div>
            <button type="submit" class="btn w-100 bg-1 color-4">Send your message</button>
        </form>
</div>
@endsection
