@extends('layouts.app')

@section('content')
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <h2 class="mb-3">Your Blog Posts</h2>
                    @forelse ($posts as $post)
                        <div class="row py-3">
                            <div class="col-9">
                                <a href="{{route('blog.show',$post->id)}}" class="color-1">{{$post->title}}</a>
                            </div>
                            <div class="col-1">
                                <a href="{{route('blog.show',$post->id)}}" class="color-1 tool" title="Show"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-1">
                                <a href="{{route('blog.edit',$post->id)}}" class="color-2 tool" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-1">
                                <a href="#"class="color-red  tool" data-toggle="modal" data-target="#Modal{{$post->id}}" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <!-- Pop-up Modal -->
                        <div class="modal fade" id="Modal{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="Modal{{$post->id}}Label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Delete Post</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    Delete {{$post->title}}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <form action="{{route('blog.destroy',$post->id)}}" method="post">
                                        @method('delete')
                                        @csrf
                                        <input class="btn color-4 bg-1" type="submit" value="Delete" />
                                    </form>

                                </div>
                            </div>
                            </div>
                        </div>
                    @empty
                        You Have No Posts
                    @endforelse
                    <div class="col-12 mt-5 mb-3">
                        <a href="{{route('blog.create')}}" class="btn w-100 color-4 bg-1 border-color-1">Add Post</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
