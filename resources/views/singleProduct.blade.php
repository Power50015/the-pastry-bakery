@extends('layouts.app')

@section('content')
<div class="container my-5">
    <div class="row">
        <div class="col-12 col-md-6">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="{{asset('img/upload/product/' .  $product->img1)}}" alt="{{$product->name}}" class="w-100" height="500">
                  </div>
                  <div class="carousel-item">
                    <img src="{{asset('img/upload/product/' .  $product->img2)}}" alt="{{$product->name}}" class="w-100" height="500">
                  </div>
                  <div class="carousel-item">
                    <img src="{{asset('img/upload/product/' .  $product->img3)}}" alt="{{$product->name}}" class="w-100" height="500">
                  </div>
                </div>
                <a class="carousel-control-prev " href="#carouselExampleControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon bg-1 color-1" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon bg-1 color-1" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
        </div>
        <div class="col-12 col-md-6">
            <h1>{{$product->name}}</h1>
            <h3 class="my-5 color-1">Price : {{$product->price}}$<h3>
            <div class="col-6">
                <form method="post" action="{{ route('cart.store')}}">
                    {{ csrf_field() }}
                    <input name="id" type="hidden" value="{{$product->id}}">
                    <input name="name" type="hidden" value="{{$product->name}}">
                    <input name="price" type="hidden" value="{{$product->price}}">
                    <div class="row">
                        <div class="col-6"><input name="qty" value="1" type="number" value="{{$product->qty}}" class="form-control"></div>
                        <div class="col-6"><button class="color-red tool btn fa fa-cart-plus font-big p-0 m-0" title="Add To Cart" type="submit">
                        </button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Hot Products</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel">
            @forelse ($alsoProducts as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/product/' .  $item->img1)}}" alt="{{$item->name}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->name}}</h4>
                        <h5>${{$item->price}}</h5>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('product.show',$item->id)}}" class="color-red tool fa fa-eye font-big" title="Show"></a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="color-red tool fa fa-cart-plus font-big" title="Add To Cart"></a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            No Prodects
            @endforelse
        </div>
    </div>
    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Blogs</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel">
            @forelse ($blogs as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/blog/' .  $item->img)}}" alt="" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->title}}</h4>
                        <a href="{{route('blog.show',$item->id)}}" class="btn color-red bg-4">Read More ...</a>
                    </div>
                </div>
            @empty
            No Blog
            @endforelse
        </div>
    </div>
</div>
@endsection
