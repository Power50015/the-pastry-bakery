<div class="index-slide w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container pt-5 float-right">
        <h1 class="text-center color-2">The Pastry<br>Bakery</h1>
        <h2 class="text-center color-1 my-5">Digtal Bakery Shop</h2>
    </div>
    <div class="clearfix"></div>
</div>