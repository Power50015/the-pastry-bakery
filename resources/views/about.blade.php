@extends('layouts.app')

@section('content')
<div class="slide-page w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container">
        <h1 class="text-center color-1">About Us</h1>
    </div>
</div>
<div class="container my-5">
    <div class="row">
        <div class="col-lg-6 col-12"><img src="{{ asset('img/sq-1.jpg') }}" alt="" class="w-100"></div>
        <div class="col-lg-6 col-12"><h3 class="my-5">Who Are We!!</h3>
        <p>In 1996, Magnolia Bakery opened its first location on a quiet street corner in the heart of New York City’s West Village. From its inception, Magnolia Bakery has been cherished for its classic American baked goods, vintage decor and warm, inviting atmosphere.
            In 2007, Magnolia Bakery’s original owner passed her oven mitts to Steve and Tyra Abrams. Together, the Abrams’ have thoughtfully expanded the bakery from its first West Village shop to locations worldwide. Today, Magnolia Bakery can be found in New York City, Los Angeles, Chicago, Boston, Washington, D.C., Dubai, Riyadh, Amman, Abu Dhabi, Manila and Doha. For more information on Magnolia Bakery’s location</p>
        </div>
    </div>
</div>
@endsection
