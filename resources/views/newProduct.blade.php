@extends('layouts.app')

@section('content')
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Product</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="name">Title</label>
                          <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" autocomplete="off" required>
                          <small id="titleHelp" class="form-text text-muted">Enter The Title of The Product</small>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="number" step="0.01" class="form-control" id="price" name="price" aria-describedby="priceHelp" autocomplete="off" required>
                            <small id="price" class="form-text text-muted">Enter The description of The Product</small>
                        </div>
                        <div class="form-group">
                            <label for="img1">Product Img 1</label>
                            <input type="file" class="form-control pb-5 border-0" name="img1" id="img1" aria-describedby="img1Help" required>
                        </div>
                        <div class="form-group">
                            <label for="img2">Product Img 2</label>
                            <input type="file" class="form-control pb-5 border-0" name="img2" id="img2" aria-describedby="img2Help" required>
                        </div>
                        <div class="form-group">
                            <label for="img3">Product Img 3</label>
                            <input type="file" class="form-control pb-5 border-0" name="img3" id="img3" aria-describedby="img3Help" required>
                        </div>
                        <div class="form-group">
                            <label for="catid">Select Category</label>
                            <select class="form-control" id="catid" name="catid" aria-describedby="catidHelp" autocomplete="off" required>
                                <option value="1">Cake</option>
                                <option value="2">Bread</option>
                                <option value="3">Pies</option>
                            </select>
                        </div>
                        <button type="submit" class="btn bg-1 color-4 w-100">Add Product</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
