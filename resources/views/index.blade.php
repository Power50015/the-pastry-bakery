@extends('layouts.app')

@section('content')
    @yield('index.slide', View::make('index.slide'))
    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Hot Product</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel my-5">
            @forelse ($products1 as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/product/' .  $item->img1)}}" alt="{{$item->name}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->name}}</h4>
                        <h5>${{$item->price}}</h5>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('product.show',$item->id)}}" class="color-red tool fa fa-eye font-big" title="Show"></a>
                            </div>
                            <div class="col-6">
                                <form method="post" action="{{ route('cart.store')}}">
                                    {{ csrf_field() }}
                                    <input name="id" type="hidden" value="{{$item->id}}">
                                    <input name="name" type="hidden" value="{{$item->name}}">
                                    <input name="qty" value="1" type="hidden" value="{{$item->qty}}">
                                    <input name="price" type="hidden" value="{{$item->price}}">
                                    <button class="color-red tool btn fa fa-cart-plus font-big p-0" title="Add To Cart" type="submit">
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            No Prodects
            @endforelse
        </div>
    </div>
    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Cakes</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel my-5">
            @forelse ($cakes as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/product/' .  $item->img1)}}" alt="{{$item->name}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->name}}</h4>
                        <h5>${{$item->price}}</h5>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('product.show',$item->id)}}" class="color-red tool fa fa-eye font-big" title="Show"></a>
                            </div>
                            <div class="col-6">
                                <form method="post" action="{{ route('cart.store')}}">
                                    {{ csrf_field() }}
                                    <input name="id" type="hidden" value="{{$item->id}}">
                                    <input name="name" type="hidden" value="{{$item->name}}">
                                    <input name="qty" value="1" type="hidden" value="{{$item->qty}}">
                                    <input name="price" type="hidden" value="{{$item->price}}">
                                    <button class="color-red tool btn fa fa-cart-plus font-big p-0" title="Add To Cart" type="submit">
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            No Cake Prodects
            @endforelse
        </div>
    </div>
    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Breads</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel my-5">
            @forelse ($breads as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/product/' .  $item->img1)}}" alt="{{$item->name}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->name}}</h4>
                        <h5>${{$item->price}}</h5>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('product.show',$item->id)}}" class="color-red tool fa fa-eye font-big" title="Show"></a>
                            </div>
                            <div class="col-6">
                                <form method="post" action="{{ route('cart.store')}}">
                                    {{ csrf_field() }}
                                    <input name="id" type="hidden" value="{{$item->id}}">
                                    <input name="name" type="hidden" value="{{$item->name}}">
                                    <input name="qty" value="1" type="hidden" value="{{$item->qty}}">
                                    <input name="price" type="hidden" value="{{$item->price}}">
                                    <button class="color-red tool btn fa fa-cart-plus font-big p-0" title="Add To Cart" type="submit">
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            No Breads Prodects
            @endforelse
        </div>
    </div>
    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Pies</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel my-5">
            @forelse ($pies as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/product/' .  $item->img1)}}" alt="{{$item->name}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->name}}</h4>
                        <h5>${{$item->price}}</h5>
                        <div class="row">
                            <div class="col-6">
                                <a href="{{route('product.show',$item->id)}}" class="color-red tool fa fa-eye font-big" title="Show"></a>
                            </div>
                            <div class="col-6">
                                <form method="post" action="{{ route('cart.store')}}">
                                    {{ csrf_field() }}
                                    <input name="id" type="hidden" value="{{$item->id}}">
                                    <input name="name" type="hidden" value="{{$item->name}}">
                                    <input name="qty" value="1" type="hidden" value="{{$item->qty}}">
                                    <input name="price" type="hidden" value="{{$item->price}}">
                                    <button class="color-red tool btn fa fa-cart-plus font-big p-0" title="Add To Cart" type="submit">
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            No Breads Prodects
            @endforelse
        </div>
    </div>
    <div class="container product-list mb-5 mt-5">
        <h2 class="title main-font-bold color-1">Blogs</h2>
        <!-- Set up your HTML -->
        <div class="owl-carousel">
            @forelse ($blog as $item)
                <div class="item position-relative">
                    <img src="{{asset('img/upload/blog/' .  $item->img)}}" alt="{{$item->title}}" class="w-100 h-100 position-relative">
                    <div class="content position-absolute pt-5 text-center color-1">
                        <h4 class="mt-3">{{$item->title}}</h4>
                        <a href="{{route('blog.show',$item->id)}}" class="btn color-red bg-4">Read More ...</a>
                    </div>
                </div>
            @empty
            No Blog
            @endforelse
        </div>
    </div>
@endsection
    