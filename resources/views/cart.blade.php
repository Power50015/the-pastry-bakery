@extends('layouts.app')
@section('content')
<div class="slide-page w-100" style="background-image: url('{{ asset('img/slide/slide1.jpg') }}')">
    <div class="container">
        <h1 class="text-center color-1">Shopping Cart</h1>
    </div>
</div>

    <div class="container my-5">
        <h2>Your cart Products ({{ Cart::count()}})</h2>   
        <div class="row text-center mt-3 bg-1 py-4 color-4">
            <div class="col-4">Product Name</div>
            <div class="col-2">Product Price</div>
            <div class="col-2">Quantity</div>
            <div class="col-2">Total Price</div>
            <div class="col-2"></div>
        </div> 
            @forelse (Cart::content() as $item)
            <div class="row text-center bg-4 py-4 color-1">
                <a class="col-4 mt-2" href="{{route('product.show',$item->id)}}">{{ $item->name}}</a>
                <div class="col-2 mt-2">{{ $item->price}}</div>
                <div class="col-2 mt-2">{{ $item->qty}}</div>
                <div class="col-2 mt-2">{{ $item->subtotal}}</div>
                <div class="col-2 mt-2">                                            
                    <form action="{{ route('cart.destroy', $item->rowId)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn-delete" title="Remove Product" type="submit">
                        <i class="fa fa-trash"></i>
                    </button>
                    </form>
                </div>
            </div> 
            @empty
                <h3 class="color-red text-center w-100 mt-4">There is no products in the cart</h3>
            @endforelse
            <h5 class="col-6 color-1 mt-4">Total Price ({{ Cart::SubTotal()}})</h5>
            @auth    
            <form action="{{ route('order.store')}}" method="post">
                @csrf
                    <button class="btn bg-2 color-4" title="Send Order" type="submit">
                        Send Order
                    </button>
            </form>
            @else
                <h3 class="color-red text-center w-100 my-5">You have to login to add your order</h3>
            @endauth
    </div>
      
    <script>
        function changeQty(increase) {
            var qty = parseInt($('.select_number').find("input").val());
            if (!isNaN(qty)) {
                qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                $('.select_number').find("input").val(qty);
            } else {
                $('.select_number').find("input").val(1);
            }
        }

        function goToByScroll(id) {
            $('html,body').animate({
                scrollTop: $("#" + id).offset().top
            }, 'slow');
        }
    </script>
@endsection
