@extends('layouts.app')

@section('content')
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Post</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('blog.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="title">Title</label>
                          <input type="text" class="form-control" name="title" id="title" aria-describedby="titleHelp" autocomplete="off" required>
                          <small id="titleHelp" class="form-text text-muted">Enter The Title of The Post</small>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" id="description" name="description" aria-describedby="descriptionHelp" autocomplete="off" required>
                            <small id="description" class="form-text text-muted">Enter The description of The Post</small>
                        </div>
                        <div class="form-group">
                            <label for="img">Img</label>
                            <input type="file" class="form-control pb-5 border-0" name="img" id="img" aria-describedby="imgHelp" required>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <input type="hidden" name="content" class="form-control" id="content" aria-describedby="contentHelp" autocomplete="off" required >
                            <trix-editor input="content"></trix-editor>
                            <small id="content" class="form-text text-muted">Enter The Content of The Post</small>
                        </div>
                        <button type="submit" class="btn bg-1 color-4 w-100">Add Post</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
