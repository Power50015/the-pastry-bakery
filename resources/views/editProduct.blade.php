@extends('layouts.app')

@section('content')
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Product</div>
                <div class="card-body">
                    <img src="{{asset('img/upload/product/' .  $product->img1)}}" alt="{{$product->name}}" class="w-100" height="500">
                    <form method="POST" action="{{ route('product.update', $product->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="name">Title</label>
                        <input value="{{$product->name}}" type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" autocomplete="off" required>
                          <small id="titleHelp" class="form-text text-muted">Enter The Title of The Product</small>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input value="{{$product->price}}" type="number" step="0.01" class="form-control" id="price" name="price" aria-describedby="priceHelp" autocomplete="off" required>
                            <small id="price" class="form-text text-muted">Enter The description of The Product</small>
                        </div>
                        <div class="form-group">
                            <label for="catid">Select Category</label>
                            <select class="form-control" id="catid" name="catid" aria-describedby="catidHelp" autocomplete="off" required>
                                <option value="1"@if ($product->catid == 1)
                                    selected
                                @endif>Cake</option>
                                <option value="2"@if ($product->catid == 2)
                                    selected
                                @endif>Bread</option>
                                <option value="3"@if ($product->catid == 3)
                                    selected
                                @endif>Pies</option>
                            </select>
                        </div>
                        <button type="submit" class="btn bg-1 color-4 w-100">Edit Product</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
